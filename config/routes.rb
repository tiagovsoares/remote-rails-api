Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :v1, defaults: { format: :json } do
    namespace :locations do
      resources :countries, only: %i[index]
      resources :cities, only: %i[index show], param: :iso2
    end
    resources :users, only: %i[index create update destroy]
  end
end
