# frozen_string_literal: true

module V1
  module Locations
    # Controller to retrive locations from rails api db
    class CountriesController < ApplicationController
      def index
        @locations = Location.all.uniq(&:iso2)
        render :index
      end
    end
  end
end
