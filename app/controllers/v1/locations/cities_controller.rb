# frozen_string_literal: true

module V1
  module Locations
    # Controller to retrive cities from rails api db
    class CitiesController < ApplicationController
      def index
        render json: []
      end

      def show
        @locations = Location.where(iso2: params[:iso2])
      end
    end
  end
end
