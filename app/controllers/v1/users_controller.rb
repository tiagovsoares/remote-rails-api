# frozen_string_literal: true

module V1
  # Controller to retrive users from rails api db
  class UsersController < ApplicationController
    before_action :set_location, only: %i[create update]
    def index
      @users = User.all
      render :index
    end

    def create
      user = User.new(
        name: @user_params[:name],
        image: @user_params[:image],
        location: @user_location
      )
      if user.save
        @user = User.last
        render :create
      else
        head :bad
      end
    end

    def update
      @user = User.find(@user_params[:id])

      @user.update(
        name: @user_params[:name],
        image: @user_params[:image],
        location: @user_location
      )
      render :update
    end

    def destroy
      @user = User.find(params[:id])

      @user.destroy
    end

    private

    def set_location
      @user_params   = params[:user]
      @user_location = Location.find_by(
        city: @user_params[:city],
        iso2: @user_params[:country]
      )
    end
  end
end
