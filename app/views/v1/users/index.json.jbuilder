json.users @users do |user|
  json.id       user.id
  json.name     user.name
  json.image    user.image
  json.city     user.location.city
  json.country  user.location.iso2
  json.timezone user.location.timezone
end
