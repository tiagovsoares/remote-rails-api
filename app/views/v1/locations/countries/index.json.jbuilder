json.locations @locations do |location|
  json.iso2    location.iso2
  json.country location.country
end
