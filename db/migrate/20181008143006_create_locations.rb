# frozen_string_literal: true

class CreateLocations < ActiveRecord::Migration[5.2]
  def change
    create_table :locations do |t|
      t.string :city
      t.string :iso2
      t.string :iso3
      t.string :country
      t.string :province
      t.string :timezone

      t.timestamps
    end
  end
end
